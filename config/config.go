package config

import (
	"fmt"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"os"
)

var (
	defaultDSN = fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable",
		"localhost", "5432", "postgres", "test-dataart")
)

type BaseConfig struct {
	LogLevel log.Level
	DBDsn    string
}

func GetBaseConfig() BaseConfig {
	logLevel := GetEnv("LOG_LEVEL", log.InfoLevel.String())
	level, err := log.ParseLevel(logLevel)
	if err != nil {
		panic(errors.Wrap(err, "failed to parse LOG_LEVEL env var"))
	}

	result := BaseConfig{
		LogLevel: level,
		DBDsn:    GetEnv("DB_DSN", defaultDSN),
	}

	return result
}

func GetEnv(envVarName string, defaultValue string) string {
	value := os.Getenv(envVarName)

	if value == "" {
		return defaultValue
	}

	return value
}

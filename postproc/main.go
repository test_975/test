package main

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"os"
	"os/signal"
	"syscall"
	"test-dataart/storage"
	"time"
)

const (
	lastTxsToCancel = 10
)

type TxCancellerStorage interface {
	// GetLastOddTxs must return Txs sorted by id descending.
	GetLastOddTxs(limit uint64) ([]storage.Tx, error)
	CancelTx(txID int64) (didCancel bool, err error)
}

func main() {
	conf := GetPostProcConfig()

	log.SetLevel(conf.LogLevel)
	log.Info("Post-processing app started.")

	appCtx, cancelFn := context.WithCancel(context.Background())
	go listenOSSignals(cancelFn)

	db, err := storage.NewDB(conf.DBDsn)
	if err != nil {
		panic(errors.Wrap(err, "failed to connect to DB"))
	}

	period := time.Minute * time.Duration(conf.PostProcessingPeriodInMin)
	doPostProcessing(appCtx, db, period)
	log.Info("App stopped cleanly.")
}

// DoPostProcessing is a blocking method.
func doPostProcessing(ctx context.Context, db TxCancellerStorage, period time.Duration) {
	// First iteration should happen immediately.
	timer := time.NewTimer(time.Second)

	for {
		select {
		case _ = <-timer.C:
			log.Info("Starting post-processing.")

			if err := cancelLastOddTxs(db, lastTxsToCancel); err != nil {
				log.WithError(err).Error(fmt.Sprintf("Failed to cancel last %d odd Txs.", lastTxsToCancel))
			} else {
				log.Infof("Post-processing finished successfully, next in %s.", period.String())
			}

			timer.Reset(period)
		case <-ctx.Done():
			log.Info("TxProcessor stopped cleanly.")
			return
		}
	}
}

func cancelLastOddTxs(db TxCancellerStorage, n uint64) (err error) {
	defer func() {
		if rec := recover(); rec != nil {
			err, ok := rec.(error)
			if ok {
				err = errors.Wrap(err, "execution panicked")
			} else {
				log.WithField("panic_value", rec).Error("Panic happened in the TxProcessor.")
				err = errors.New("execution panicked")
			}
		}
	}()

	txs, err := db.GetLastOddTxs(n)
	if err != nil {
		return errors.Wrap(err, "failed to get last odd Txs from DB")
	}

	for _, tx := range txs {
		if err := cancelTx(db, tx); err != nil {
			return errors.Wrap(err, fmt.Sprintf("failed to cancel Tx (%d)", tx.ID))
		}
	}

	return nil
}

// CancelTx doesn't return error if Tx is cancelled already or insufficient balance to cancel.
func cancelTx(db TxCancellerStorage, tx storage.Tx) error {
	if tx.IsCancelled {
		log.WithField("tx", tx).Debug("Tx is already cancelled, skipping.")
		return nil
	}

	didCancel, err := db.CancelTx(tx.ID)
	if err != nil {
		switch err {
		case storage.ErrInsufficientBalance:
			log.WithFields(log.Fields{
				"tx": tx,
			}).Info("Tx was not cancelled due to insufficient balance.")
			return nil
		default:
			return errors.Wrap(err, "failed to cancel Tx in the DB")
		}
	}
	tx.IsCancelled = true

	if didCancel {
		log.WithFields(log.Fields{
			"tx": tx,
		}).Info("Tx has just been cancelled, balance updated.")
	} else {
		log.WithFields(log.Fields{
			"tx": tx,
		}).Debug("Tx is already cancelled.")
	}

	return nil
}

func listenOSSignals(cancelFn func()) {
	signalsToListen := []os.Signal{
		syscall.SIGTERM,
		syscall.SIGINT,
	}

	var osSignals = make(chan os.Signal)
	for _, s := range signalsToListen {
		signal.Notify(osSignals, s)
	}

	// Waiting for an OS Signal
	sig := <-osSignals
	log.WithField("signal", sig).Info("App received signal - cancelling context.")
	cancelFn()
}

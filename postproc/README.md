This application periodically runs "post-processing" which cancels 10 last odd transactions (with current balance recalculating).

## Configuration environment variables
* `LOG_LEVEL` - optional string (`info` by default), can be one of `panic`, `fatal`, `error`, `warn`, `info`, `debug` or `trace`.
* `DB_DSN` - optional string (`host=localhost port=5432 user=postgres dbname=test-dataart sslmode=disable` by default), sets the DSN line for the Postgres DB connection, can also contain password.
* `POST_PROCESSING_PERIOD_IN_MIN` - optional integer (`1` by default), period of post-processing, in minutes

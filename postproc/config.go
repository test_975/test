package main

import (
	"github.com/pkg/errors"
	"strconv"
	"test-dataart/config"
)

type PostProcConfig struct {
	config.BaseConfig
	PostProcessingPeriodInMin int
}

func GetPostProcConfig() PostProcConfig {
	result := PostProcConfig{
		BaseConfig: config.GetBaseConfig(),
	}

	periodStr := config.GetEnv("POST_PROCESSING_PERIOD_IN_MIN", "1")
	period, err := strconv.Atoi(periodStr)
	if err != nil {
		panic(errors.Wrap(err, "failed to parse integer from the POST_PROCESSING_PERIOD_IN_MIN"))
	}

	result.PostProcessingPeriodInMin = period
	return result
}

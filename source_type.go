package main

import "errors"

type SourceType string

const (
	GameSourceType SourceType = "game"
	ServerSourceType SourceType = "server"
	PaymentSourceType SourceType = "payment"
)

var (
	AvailableSourceTypes = []SourceType {
		GameSourceType,
		ServerSourceType,
		PaymentSourceType,
	}
)

func (t SourceType) Validate() error {
	for _, at := range AvailableSourceTypes {
		if t == at {
			return nil
		}
	}

	return errors.New("source type is not among available source types")
}

-- +migrate Up

CREATE TABLE transaction (
    id serial primary key,

    amount bigint not null,
    transaction_id varchar unique not null,
    is_cancelled bool not null
--     accepted_at timestamp without time zone not null,
);

CREATE TABLE balance (
    value bigint not null
)
INSERT INTO balance values(0);


-- +migrate Down

DROP TABLE IF EXISTS transaction cascade;
DROP TABLE IF EXISTS balance cascade;

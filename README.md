This application accepts POST requests to the `/v1/transactions` url with `win` and `lost` Transactions
which increase and decrease the balance accordingly (HTTP request is described below).

Description of the ost-processing application for cancelling Transactions can be found [here](./postproc/README.md).

Necessary tables in DB can be created by executing the `-- +migrate Up` section from the `001_migration.sql` file.

## Configuration environment variables
* `LOG_LEVEL` - optional string (`info` by default), can be one of `panic`, `fatal`, `error`, `warn`, `info`, `debug` or `trace`.
* `SERVER_ADDRESS` - optional string (`:8000` by default), describes the address to start server at in format `host:port` (host can be omitted).
* `DB_DSN` - optional string (`host=localhost port=5432 user=postgres dbname=test-dataart sslmode=disable` by default), sets the DSN line for the Postgres DB connection, can also contain password.

### Request headers

* `Source-Type` - required, must be either `game`, `server` or `payment`.
* `Content-Type` - optional (`application/json` by default), if provided must be `application/json`

### Request body arguments

|  name  |  notes  | description | example |
| ------ | ------- | ----------- | ------- |
| `state` | required, string | State of the Transaction: `win` or `lost` | `win` |
| `amount` | required, string, float value with precision 2, positive number only | Amount of the Transaction. | `10.15` |
| `transactionId` | required, string | Unique identifier of a Transaction. | `my_awesome_generated_id` |

### Example Request Body:
```json
{
    "state": "win", 
    "amount": "10.15", 
    "transactionId": "my_awesome_generated_id"
}
```

### Possible specific error response codes

* Forbidden(403) - if it's not enough balance to fulfil the `lost` Transaction
* Conflict(409) - if Transactions with the same `transactionId` already exists.

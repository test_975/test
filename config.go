package main

import (
	"os"
	"test-dataart/config"
)

const (
	defaultServerAddress = ":8000"
)

type Config struct {
	config.BaseConfig
	ServerAddress           string
}

func GetConfig() Config {
	return Config{
		BaseConfig:    config.GetBaseConfig(),
		ServerAddress: getEnv("SERVER_ADDRESS", defaultServerAddress),
	}
}

func getEnv(envVarName string, defaultValue string) string {
	value := os.Getenv(envVarName)

	if value == "" {
		return defaultValue
	}

	return value
}

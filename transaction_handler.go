package main

import (
	"encoding/json"
	"github.com/google/jsonapi"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"strconv"
	"test-dataart/storage"
)

type TxSaverStorage interface {
	Save(amount int64, txID string) (id int64, err error)
}

func handleTransaction(w http.ResponseWriter, r *http.Request) {
	_, err := getSourceTypeHeader(r)
	if err != nil {
		renderError(w, err.Error(), http.StatusBadRequest)
		return
	}
	// TODO Authorization of requests (if any) should be here (as requests come from 3rd parties), however request example doesn't contain authorization.

	req, err := readRequest(r)
	if err != nil {
		renderError(w, err.Error(), http.StatusBadRequest)
		return
	}

	signedAmount := req.Amount
	if req.State == LostTxState {
		signedAmount = -signedAmount
	}

	rowID, err := db.Save(signedAmount, req.TransactionID)
	if err != nil {
		switch err {
		case storage.ErrUniqueConstraint:
			renderError(w, err.Error(), http.StatusConflict)
		case storage.ErrInsufficientBalance:
			renderError(w, err.Error(), http.StatusForbidden)
		default:
			log.WithError(err).Error("Got unexpected error from the TxProcessor, 500 is being returned.")
			renderError(w, "server error occurred", http.StatusInternalServerError)
		}

		return
	}

	log.WithFields(log.Fields{
		"tx_request": req,
		"tx_db_id":   rowID,
	}).Info("Transaction saved.")

	// Tx was applied successfully.
	w.Header().Set("Content-Type", "application/hal+json; charset=utf-8")
	w.WriteHeader(http.StatusNoContent)
}

func getSourceTypeHeader(r *http.Request) (*SourceType, error) {
	values := r.Header["Source-Type"]

	if len(values) != 1 {
		return nil, errors.New("it must be exactly 1 Source-Type header")
	}

	result := SourceType(values[0])
	if err := result.Validate(); err != nil {
		return nil, errors.Wrap(err, "Source-Type header has invalid value")
	}

	return &result, nil
}

func readRequest(r *http.Request) (*TxRequest, error) {
	bb, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(errors.Wrap(err, "failed to read request body"))
	}

	var result TxRequest
	if err := json.Unmarshal(bb, &result); err != nil {
		return nil, errors.Wrap(err, "failed to parse requests from JSON")
	}

	if err := result.Validate(); err != nil {
		return nil, err
	}

	return &result, nil
}

func renderError(w http.ResponseWriter, title string, status int) {
	w.Header().Set("Content-Type", "application/hal+json; charset=utf-8")
	w.WriteHeader(status)

	t := jsonapi.ErrorObject{
		Title:  title,
		Status: strconv.Itoa(status),
	}

	// Error is ignored intentionally is it can't be anyhow processed here.
	err := jsonapi.MarshalErrors(w, []*jsonapi.ErrorObject{&t})
	if err != nil {
		panic(errors.Wrap(err, "failed to marshal JsonAPI errors into response"))
	}
}

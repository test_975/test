package storage

import (
	"github.com/lib/pq"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"

	"github.com/Masterminds/squirrel"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

const (
	uniqueViolationErrCode = "23505"
	serializeErrCode       = "40001"

	txTable      = "transaction"
	balanceTable = "balance"
)

var (
	ErrUniqueConstraint    = errors.New("tx with such TransactionID exists already")
	ErrInsufficientBalance = errors.New("balance too low")

	psql           = squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar)
	errSerializing = errors.New("could not serialize access due to concurrent update")
)

type DB struct {
	db *sqlx.DB
}

func NewDB(dsn string) (*DB, error) {
	db, err := sqlx.Open("postgres", dsn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to open sqlx")
	}

	if err = db.Ping(); err != nil {
		return nil, errors.Wrap(err, "failed to ping DB")
	}

	return &DB{
		db: db,
	}, nil
}

func (d *DB) GetLastOddTxs(limit uint64) ([]Tx, error) {
	query, args := psql.Select("*").From(txTable).Where("(id % 2) = 1").
		OrderBy("id desc").Limit(limit).MustSql()
	result := make([]Tx, 0, limit)

	if err := d.db.Select(&result, query, args...); err != nil {
		log.WithField("sql_query", query).WithField("args", args).WithError(err).
			Error("Failed to select from DB getting last odd Txs.")
		return nil, err
	}

	return result, nil
}

func (d *DB) Save(amount int64, transactionID string) (id int64, err error) {
	for {
		id, err := d.trySave(amount, transactionID)
		if err != nil {
			switch err {
			case errSerializing:
				// Try again, Tx was failed to commit due to concurrent updates.
				// No waiting here, because other concurrent SQL transaction has committed already.
				continue
			default:
				// The error must not be modified(wrapped) as it can be one of the public error vars stated at the top.
				return 0, err
			}
		}

		return id, nil
	}
}

func (d *DB) trySave(amount int64, transactionID string) (id int64, err error) {
	err = d.beginTransaction()
	if err != nil {
		return 0, err
	}

	curBalance, err := d.getBalance()
	if err != nil {
		return 0, errors.Wrap(err, "failed to get current balance")
	}
	if curBalance+amount < 0 {
		// Error from ROLLBACK is ignore intentionally (we can't handle it anyway).
		_, _ = d.db.Exec("ROLLBACK;")
		return 0, ErrInsufficientBalance
	}

	err = d.updateBalance(amount)
	if err != nil {
		if err == errSerializing {
			// Error from ROLLBACK is ignore intentionally (we can't handle it anyway).
			_, _ = d.db.Exec("ROLLBACK;")
			return 0, errSerializing
		} else {
			return 0, errors.Wrap(err, "failed to update Balance")
		}
	}

	id, err = d.insertTx(amount, transactionID)
	if err != nil {
		switch err {
		case ErrUniqueConstraint:
			return 0, ErrUniqueConstraint
		case errSerializing:
			// Error from ROLLBACK is ignore intentionally (we can't handle it anyway).
			_, _ = d.db.Exec("ROLLBACK;")
			return 0, errSerializing
		}

		return 0, errors.Wrap(err, "failed to insert Tx")
	}

	_, err = d.db.Exec("COMMIT;")
	if err != nil {
		return 0, errors.Wrap(err, "failed to commit SQL transaction")
	}

	return id, nil
}

func (d *DB) beginTransaction() error {
	_, err := d.db.Exec("BEGIN TRANSACTION ISOLATION LEVEL REPEATABLE READ;")
	if err != nil {
		if pqErr, ok := err.(*pq.Error); ok {
			log.WithField("pqerror_code", pqErr.Code).WithError(pqErr).Warn("Failed to begin SQL transaction.")

			// Error from ROLLBACK is ignore intentionally (we can't handle it anyway).
			_, _ = d.db.Exec("ROLLBACK;")

			_, err := d.db.Exec("BEGIN TRANSACTION ISOLATION LEVEL REPEATABLE READ;")
			return errors.Wrap(err, "failed to begin a REPEATABLE READ transaction")
		}

		return errors.Wrap(err, "failed to begin a REPEATABLE READ transaction")
	}

	return nil
}

func (d *DB) insertTx(amount int64, transactionID string) (id int64, err error) {
	query, args, err := psql.Insert(txTable).Columns("amount", "transaction_id", "is_cancelled").
		Values(amount, transactionID, false).Suffix(" returning id").ToSql()
	if err != nil {
		log.WithFields(log.Fields{
			"sql_query": query,
			"args":      args,
		}).WithError(err).Error("Failed to build insert query for saving Tx.")
		return 0, errors.Wrap(err, "failed to build insert query to save Tx")
	}

	var result int64
	query = d.db.Rebind(query)
	err = d.db.Get(&result, query, args...)
	if err != nil {
		if pqErr, ok := err.(*pq.Error); ok {
			switch pqErr.Code {
			case uniqueViolationErrCode:
				return 0, ErrUniqueConstraint
			case serializeErrCode:
				return 0, errSerializing
			}
		}

		log.WithFields(log.Fields{
			"sql_query": query,
			"args":      args,
		}).WithError(err).Error("Failed to exec into DB.")
		return 0, errors.Wrap(err, "failed to exec into DB to save Tx")
	}

	return result, nil
}

// CancelTx returns false,nil if the Tx is already cancelled and
// ErrInsufficientBalance error if the Tx can't be cancelled due to low balance.
func (d *DB) CancelTx(txID int64) (didCancel bool, err error) {
	for {
		didCancel, err := d.tryCancelTx(txID)
		if err != nil {
			switch err {
			case errSerializing:
				// Try again, Tx was failed to commit due to concurrent updates.
				// No waiting here, because other concurrent SQL transaction has committed already.
				continue
			default:
				// The error must not be modified(wrapped) as it can be one of the public error vars stated at the top.
				return false, err
			}
		}

		return didCancel, nil
	}
}

func (d *DB) tryCancelTx(txID int64) (didCancel bool, err error) {
	err = d.beginTransaction()
	if err != nil {
		return false, err
	}

	tx, err := d.getTx(txID)
	if err != nil {
		return false, errors.Wrap(err, "failed to get the Tx from DB")
	}
	if tx.IsCancelled {
		return false, nil
	}

	curBalance, err := d.getBalance()
	if err != nil {
		return false, errors.Wrap(err, "failed to get current balance")
	}
	// Using minus to revert the effect of the Tx
	if curBalance - tx.Amount < 0 {
		// Error from ROLLBACK is ignore intentionally (we can't handle it anyway).
		_, _ = d.db.Exec("ROLLBACK;")
		return false, ErrInsufficientBalance
	}

	err = d.updateBalance(-tx.Amount)
	if err != nil {
		if err == errSerializing {
			// Error from ROLLBACK is ignore intentionally (we can't handle it anyway).
			_, _ = d.db.Exec("ROLLBACK;")
			return false, errSerializing
		} else {
			return false, errors.Wrap(err, "failed to update Balance")
		}
	}

	err = d.updateTxToCancelled(txID)
	if err != nil {
		if err == errSerializing {
			// Error from ROLLBACK is ignore intentionally (we can't handle it anyway).
			_, _ = d.db.Exec("ROLLBACK;")
			return false, errSerializing
		} else {
			return false, errors.Wrap(err, "failed to update Tx to cancelled")
		}
	}

	_, err = d.db.Exec("COMMIT;")
	if err != nil {
		return false, errors.Wrap(err, "failed to commit SQL transaction")
	}

	return true, nil
}

func (d *DB) getTx(txID int64) (*Tx, error) {
	query, args := psql.Select("*").From(txTable).Where("id = ?", txID).MustSql()
	result := &Tx{}

	if err := d.db.Get(result, query, args...); err != nil {
		log.WithFields(log.Fields{
			"sql_query": query,
			"args":      args,
		}).WithError(err).Errorf("Failed to Get single row from the %s table.", txTable)
		return nil, errors.Wrap(err, "failed to Get single row from the transaction table")
	}

	return result, nil
}

func (d *DB) updateTxToCancelled(txID int64) error {
	query, args, err := psql.Update(txTable).Set("is_cancelled", true).Where("id = ?", txID).ToSql()
	if err != nil {
		log.WithFields(log.Fields{
			"sql_query": query,
			"args":      args,
		}).WithError(err).Error("Failed to build update query for cancelling Tx.")
		return errors.Wrap(err, "failed to build update query")
	}

	_, err = d.db.Exec(query, args...)
	if err != nil {
		if pqErr, ok := err.(*pq.Error); ok {
			if pqErr.Code == serializeErrCode{
				return errSerializing
			}
		}

		log.WithFields(log.Fields{
			"sql_query": query,
			"args":      args,
		}).WithError(err).Error("Failed to exec update into DB for setting Tx to cancelled.")
		return errors.Wrap(err, "failed to exec update into DB for setting Tx to cancelled")
	}

	return nil
}

type balance struct {
	Value int64 `db:"value"`
}

func (d *DB) getBalance() (int64, error) {
	query, _ := psql.Select("*").From(balanceTable).Limit(1).MustSql()
	result := &balance{}

	if err := d.db.Get(result, query); err != nil {
		log.WithFields(log.Fields{
			"sql_query": query,
		}).WithError(err).Error("Failed to Get single row from the balance table.")
		return 0, errors.Wrap(err, "failed to Get single row from the balance table")
	}

	return result.Value, nil
}

func (d *DB) updateBalance(diff int64) error {
	query, args, err := psql.Update(balanceTable).Set("value", squirrel.Expr("value + ?", diff)).ToSql()
	if err != nil {
		log.WithFields(log.Fields{
			"sql_query": query,
			"args":      args,
		}).WithError(err).Error("Failed to build update query for updating Balance.")
		return errors.Wrap(err, "failed to build update query for updating Balance")
	}

	_, err = d.db.Exec(query, args...)
	if err != nil {
		if pqErr, ok := err.(*pq.Error); ok {
			if pqErr.Code == serializeErrCode{
				return errSerializing
			}
		}

		log.WithFields(log.Fields{
			"sql_query": query,
			"args":      args,
		}).WithError(err).Error("Failed to exec update into DB for updating Balance.")
		return errors.Wrap(err, "failed to exec update into DB for updating Balance")
	}

	return nil
}

package storage

type Tx struct {
	ID int64 `db:"id"`
	// Amount is a signed value, where sign denotes the win/lost state.
	Amount        int64  `db:"amount"`
	TransactionID string `db:"transaction_id"`
	IsCancelled   bool   `db:"is_cancelled"`
}

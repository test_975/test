package main

import (
	"context"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"test-dataart/storage"
	"time"

	"github.com/go-chi/chi"
)

var (
	db TxSaverStorage
)

func main() {
	conf := GetConfig()

	log.SetLevel(conf.LogLevel)
	log.Info("App started.")

	appCtx, cancelFn := context.WithCancel(context.Background())
	go listenOSSignals(cancelFn)

	router := prepareRouter()

	var err error
	db, err = storage.NewDB(conf.DBDsn)
	if err != nil {
		panic(errors.Wrap(err, "failed to connect to DB"))
	}

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		runServer(appCtx, conf.ServerAddress, 30*time.Second, router)
		wg.Done()
	}()

	wg.Wait()
	log.Info("App stopped cleanly.")
}

func listenOSSignals(cancelFn func()) {
	signalsToListen := []os.Signal{
		syscall.SIGTERM,
		syscall.SIGINT,
	}

	var osSignals = make(chan os.Signal)
	for _, s := range signalsToListen {
		signal.Notify(osSignals, s)
	}

	// Waiting for an OS Signal
	sig := <-osSignals
	log.WithField("signal", sig).Info("App received signal - cancelling context.")
	cancelFn()
}

func prepareRouter() http.Handler {
	r := chi.NewRouter()

	r.Use(RecoverMiddleware)
	r.Use(JsonContentMiddleware)

	r.Route("/v1", func(r chi.Router) {
		r.Post("/transactions", handleTransaction)
	})

	return r
}

// RunServer is a blocking function.
func runServer(ctx context.Context, address string, reqWriteTimeout time.Duration, handler http.Handler) {
	server := &http.Server{
		Addr:         address,
		Handler:      handler,
		WriteTimeout: reqWriteTimeout,
	}

	go func() {
		log.Infof("Starting Server (listening on '%s').", address)
		err := server.ListenAndServe()
		if err != http.ErrServerClosed {
			panic(errors.Wrap(err, "server listening failed with the error"))
		}
	}()

	<-ctx.Done()

	_ = server.Shutdown(context.Background())
	log.Info("Server stopped.")
}

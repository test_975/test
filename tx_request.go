package main

import (
	"encoding/json"
	"github.com/pkg/errors"
	"math"
	"strconv"
	"strings"
)

const (
	decimalSeparator = "."
	amountPrecision = 2
)

var (
	amountOne = int(math.Round(math.Pow10(amountPrecision)))
)

type TxRequest struct {
	State  TxState `json:"state"`
	// Amount is always positive value.
	Amount        int64  `json:"amount,string"`
	TransactionID string `json:"transactionId"`
}
type Alias TxRequest

func (r *TxRequest) UnmarshalJSON(data []byte) error {
	t := &struct {
		*Alias
		Amount string `json:"amount"`
	}{
		Alias: (*Alias)(r),
	}

	if err := json.Unmarshal(data, t); err != nil {
		return err
	}

	amount, err := parseIntFromFloat(t.Amount)
	if err != nil {
		return errors.Wrap(err, "amount is invalid")
	}

	r.Amount = int64(amount)
	return nil
}

func (r TxRequest) Validate() error {
	if err := r.State.Validate(); err != nil {
		return errors.Wrap(err, "state is invalid")
	}

	if r.Amount <= 0 {
		return errors.New("amount must be a positive value")
	}

	return nil
}

func parseIntFromFloat(s string) (int, error) {
	parts := strings.Split(s, decimalSeparator)
	if len(parts) == 0 {
		return 0, errors.New("must not be empty")
	}

	if len(parts) > 2 {
		return 0, errors.New("too many separated parts")
	}

	amount, err := strconv.Atoi(parts[0])
	if err != nil {
		return 0, errors.New("failed to parse numeric value from integer part")
	}
	amount = amount * amountOne

	if len(parts) > 1 {
		fracStr := parts[1]
		if len(fracStr) > amountPrecision {
			fracStr = fracStr[:amountPrecision]
		}
		missingZeros := amountPrecision - len(fracStr)
		fracStr += strings.Repeat("0", missingZeros)

		fracPart, err := strconv.Atoi(fracStr)
		if err != nil {
			return 0, errors.New("failed to parse numeric value from fractional part")
		}

		amount += fracPart
	}

	return amount, nil
}

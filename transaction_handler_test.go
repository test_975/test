package main

import (
	"bytes"
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"test-dataart/storage"
	"testing"

	"github.com/stretchr/testify/mock"
)

func TestTxHandleTransaction(t *testing.T) {
	dbMock := &TxSaverStorageMock{}
	db = dbMock

	tests := []struct {
		name    string

		body    string
		headers map[string]string
		prepare func()

		code    int
	}{
		// BadRequests
		{"missing Source-Type header", `{"state": "rubbish", "amount": "10.15", "transactionId": "26"}`,
			map[string]string{}, func(){}, http.StatusBadRequest},
		{"invalid Source-Type header", `{"state": "rubbish", "amount": "10.15", "transactionId": "26"}`,
			map[string]string{"Source-Type": "invalid_value"}, func(){}, http.StatusBadRequest},
		{"invalid state", `{"state": "rubbish", "amount": "10.15", "transactionId": "26"}`,
			map[string]string{"Source-Type": "game"}, func(){}, http.StatusBadRequest},
		{"non-number amount", `{"state": "win", "amount": "non-number", "transactionId": "26"}`,
			map[string]string{"Source-Type": "game"}, func(){}, http.StatusBadRequest},
		{"negative amount", `{"state": "win", "amount": "-12.3", "transactionId": "26"}`,
			map[string]string{"Source-Type": "game"}, func(){}, http.StatusBadRequest},


		{"duplicate TransactionID", `{"state": "win", "amount": "12.3", "transactionId": "26"}`,
			map[string]string{"Source-Type": "game"}, func(){
			dbMock.On("Save", int64(1230), "26").Return(int64(0), storage.ErrUniqueConstraint).Once()
		}, http.StatusConflict},

		{"balance too low", `{"state": "win", "amount": "12.3", "transactionId": "26"}`,
			map[string]string{"Source-Type": "game"}, func(){
			dbMock.On("Save", int64(1230), "26").Return(int64(0), storage.ErrInsufficientBalance).Once()
		}, http.StatusForbidden},

		{"some general error", `{"state": "win", "amount": "12.3", "transactionId": "26"}`,
			map[string]string{"Source-Type": "game"}, func(){
			dbMock.On("Save", int64(1230), "26").Return(int64(0), errors.New("general error")).Once()
		}, http.StatusInternalServerError},

		{"success", `{"state": "win", "amount": "12.3", "transactionId": "26"}`,
			map[string]string{"Source-Type": "game"}, func(){
			dbMock.On("Save", int64(1230), "26").Return(int64(0), nil).Once()
		}, http.StatusNoContent},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.prepare()

			w := httptest.NewRecorder()
			r := httptest.NewRequest("POST", "http://example.com", bytes.NewReader([]byte(test.body)))

			reqHeader := map[string][]string{}
			for k, v := range test.headers {
				reqHeader[k] = []string{v}
			}
			r.Header = reqHeader

			handleTransaction(w, r)

			assertResponse(t, w, test.code)
		})
	}
}

func assertResponse(t *testing.T, w *httptest.ResponseRecorder, expectedCode int) {
	if w.Code != expectedCode {
		bb, err := ioutil.ReadAll(w.Body)
		t.Log("response_body", string(bb), err)
		t.Errorf("got %d code, want %d", w.Code, expectedCode)
		return
	}
}

type TxSaverStorageMock struct {
	mock.Mock
}

func (m *TxSaverStorageMock) Save(amount int64, transactionID string) (id int64, err error) {
	aa := m.Called(amount, transactionID)
	return aa.Get(0).(int64) , aa.Error(1)
}

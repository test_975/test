module test-dataart

go 1.12

require (
	github.com/Masterminds/squirrel v1.1.0
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/google/jsonapi v0.0.0-20181016150055-d0428f63eb51
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.0.0
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.2.2
)

package main

import "errors"

type TxState string

const (
	WinTxState TxState = "win"
	LostTxState TxState = "lost"
)

func (s TxState) Validate() error {
	if s != WinTxState && s != LostTxState {
		return errors.New("only 'win' and 'lost' values are allowed")
	}

	return nil
}

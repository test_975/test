package main

import (
	"github.com/google/jsonapi"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strconv"
)

func RecoverMiddleware(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if rec := recover(); rec != nil {
				log.WithField("panic_value", rec).Error("Panic happened during processing of the request.")

				w.Header().Set("Content-Type", "application/hal+json; charset=utf-8")
				w.WriteHeader(http.StatusInternalServerError)

				t := jsonapi.ErrorObject{
					Title:  "Something really unexpected happened.",
					Status: strconv.Itoa(http.StatusInternalServerError),
				}
				// Error is ignored intentionally as it can't be anyhow handled here.
				_ = jsonapi.MarshalErrors(w, []*jsonapi.ErrorObject{&t})
			}
		}()

		h.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

func JsonContentMiddleware(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		values := r.Header["Content-Type"]

		if len(values) != 0 && values[0] != "application/json" {
			renderError(w, "only application/json Content-Type is acceptable", http.StatusNotAcceptable)
			return
		}

		// Content-Type is application/json or not provided (in this case application/json is used as default).
		h.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}
